using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api_service.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace app.Controllers
{
    [ApiController]
    [Route("/")]
    public class DevController : ControllerBase 
    {
        private readonly ILogger<DevController> _logger;
        private readonly EnvironmentModel _model;
        private readonly IHost _host;
        private readonly AppDBContext _context;
        private readonly IWebHostEnvironment _env;

        public DevController(ILogger<DevController> logger, EnvironmentModel model, IHost host, AppDBContext context,  IWebHostEnvironment env)
        {
            _logger = logger;
            _model = model;
            _host = host;
            _context = context;
            _env = env;
        }

        [HttpGet]
        public ActionResult Health()
        {
            return Ok("Privet");
        }

        [HttpGet]
        [Route("environment")]
        public EnvironmentModel GetInfo()
        {
            return  _env.IsDevelopment() ? _model : null;
        }

        [HttpGet]
        [Route("migrate")]
        public ActionResult DbMigrate()
        {
            try {
                _context.Database.Migrate();
                return Ok("OK");
            } catch {
                return Ok("Error");
            }
        }
    }
}