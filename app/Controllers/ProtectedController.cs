using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace app.Controllers
{
    //[Authorize(Policy = "ApiPublic")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProtectedController : Controller
    {
        // [Authorize(Roles = "user")]
        [HttpGet("user")]
        public ActionResult<IEnumerable<string>> GetUser()
        {
            return new JsonResult(User.Claims.Select(c => new { c.Type, c.Value }));
        }
        
        [Authorize(Roles = "admin")]
        [HttpGet("admin")]
        public ActionResult<IEnumerable<string>> GetAdmin()
        {
            return new JsonResult(User.Claims.Select(c => new { c.Type, c.Value }));
        }
    }
}