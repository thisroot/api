namespace IdentityService.Models.Identity
{
    public class BasicUserClaims
    {
        private BasicUserClaims(string value)
        {
            Value = value;
        }
        public string Value { get; set; }

        public static BasicUserClaims AdminApi => new BasicUserClaims("api.admin");
        public static BasicUserClaims PublicApi => new BasicUserClaims("api.public");
    }
}