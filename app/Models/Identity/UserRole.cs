namespace IdentityService.Models.Identity
{
    public class UserRoles
    {
        public static string User => "user";
        public static string Admin => "admin";
    }
}