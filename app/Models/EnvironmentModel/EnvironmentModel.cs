using System;

namespace api_service.Models
{
    public class EnvironmentModel 
    {
        public string AppEnvironment { get; set; }
        public string AppHost { get; set; }
        public string AuthorizationUrl { get; set; }
        public DBConnection DB { get; set;  }

        public string getConnectionString()
        {
            return String.Format("Host={0};Port={1};Database={2};Username={3};Password={4}", DB.Host, DB.Port, DB.Database, DB.User, DB.Password);
        }

    }
}