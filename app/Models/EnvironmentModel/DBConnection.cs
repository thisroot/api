namespace api_service.Models
{
    public class DBConnection
    {
        public string User { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public string Host { get; set; }
        
        public string Port { get; set; }
    }
}