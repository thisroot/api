namespace api_service.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string WWW { get; set; }
    }
}