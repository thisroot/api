using System;
using api_service.Models;
using Microsoft.Extensions.Configuration;

namespace IdentityService.Services
{
    public class SettingsService
    {
        public EnvironmentModel env;
        
        public SettingsService()
        {
            var environmentName = GetEnvironmentVariable("DOTNET_ENVIRONMENT");
            var basePath = AppContext.BaseDirectory;
            var builder = new ConfigurationBuilder()
                .SetBasePath(basePath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{environmentName}.json", true)
                .AddEnvironmentVariables();

            var config = builder.Build();

            env = new EnvironmentModel
            {
                AppEnvironment = GetEnvironmentVariable("ENVIRONMENT") ?? "Development",
                AppHost = GetEnvironmentVariable("APPHOST") ?? "local",
                DB = new DBConnection
                {
                    Database = GetEnvironmentVariable("DB") ??
                               config.GetValue<string>("DBConnection:Database"),
                    Host = GetEnvironmentVariable("DB_HOST") ?? config.GetValue<string>("DBConnection:Host"),
                    Port = GetEnvironmentVariable("DB_PORT") ?? config.GetValue<string>("DBConnection:Port"),
                    Password = GetEnvironmentVariable("DB_PASSWORD") ??
                               config.GetValue<string>("DBConnection:Password"),
                    User = GetEnvironmentVariable("DB_USER") ?? config.GetValue<string>("DBConnection:User")
                },
                AuthorizationUrl = GetEnvironmentVariable("AUTH_URL") ?? config.GetValue<string>("Authorization:Url")
            };
        }
        
        private string GetEnvironmentVariable(string name)
        {
            return Environment.GetEnvironmentVariable(name.ToLower()) ?? Environment.GetEnvironmentVariable(name.ToUpper());
        }
    }
}