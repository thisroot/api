 ### Migration database policy
 
 - local (development)
     - `dotnet ef migrations add "migration name'`
     - `dotnet ef database update`
 - k8s (development / production)
     - `dotnet ef migrations add "migration name'`
     - GET /development/migrate
 
 
 ### Doker
 - `docker build -t aspnetapp .`
 - `docker run -d -p 8080:80 --name myapp aspnetapp`
 
 ### Kubernetes
 kubectl get all -n api-production
 kubectl port-forward service/api-helm 9999:80 -n api-production