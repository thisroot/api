FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine as build
WORKDIR /app
EXPOSE 80


# copy csproj and restore
COPY app/*.csproj ./aspnetapp/
RUN cd ./aspnetapp/ && dotnet restore 

# copy all files and build
COPY app/. ./aspnetapp/
WORKDIR /app/aspnetapp
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/sdk:5.0-alpine as runtime
WORKDIR /app
COPY --from=build /app/aspnetapp/out ./

ENV ASPNETCORE_URLS=http://+:80  
ENV DOTNET_USE_POLLING_FILE_WATCHER=true

ENTRYPOINT [ "dotnet", "api-service.dll" ]

